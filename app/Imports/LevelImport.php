<?php

namespace App\Imports;

use App\Models\LevelModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class LevelImport implements ToCollection
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        // 循环遍历数据
        foreach ($collection as $row) {
            $item = $row->toArray();
            $data = [
                'name' => $item[0],
                'status' => $item[1] == "正常" ? 1 : 2,
                'sort' => $item[2]
            ];
            // 插入数据
            $levelModel = new LevelModel();
            $levelModel->edit($data);
        }
    }
}
