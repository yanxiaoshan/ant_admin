<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2021 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace App\Services;

use App\Models\ConfigDataModel;

/**
 * 配置管理-服务类
 * @author 牧羊人
 * @since 2020/11/11
 * Class ConfigService
 * @package App\Services
 */
class ConfigDataService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/11/11
     * ConfigService constructor.
     */
    public function __construct()
    {
        $this->model = new ConfigDataModel();
    }

    /**
     * 获取配置列表
     * @return array
     * @since 2020/11/11
     * @author 牧羊人
     */
    public function getList()
    {
        $param = request()->all();
        // 查询条件
        $map = [];
        // 配置ID
        $configId = getter($param, "configId", 0);
        if ($configId) {
            $map[] = ['config_id', '=', $configId];
        }
        // 配置标题
        $title = getter($param, "title");
        if ($title) {
            $map[] = ['name', 'title', "%{$title}%"];
        }
        $list = $this->model->getList($map, [['sort', 'asc']]);
        return message("操作成功", true, $list);
    }

}
